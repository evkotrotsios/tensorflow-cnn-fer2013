import numpy as np
import matplotlib.pyplot as plt

image_height    = 48
image_width     = 48
class_names     = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']


def load_data(dataset_path, normalize=True, reshape=False):
    train_images    = []
    train_labels    = []
    test_images     = []
    test_labels     = []

    with open(dataset_path, 'r') as file:
        for line_no, line in enumerate(file.readlines()):
            if 0 < line_no <= 35887:
                curr_class, line, set_type  = line.split(',')
                image_data                  = np.asarray([int(x) for x in line.split()]).reshape(image_height, image_width)

                if normalize is True:
                    image_data = image_data.astype(np.float32) / 255

                if set_type.strip() == 'PrivateTest':
                    test_images.append(image_data)
                    test_labels.append(curr_class)
                else:
                    train_images.append(image_data)
                    train_labels.append(curr_class)

    train_labels    = list(map(int, train_labels))
    test_labels     = list(map(int, test_labels))
    train_images    = np.array(train_images)
    train_labels    = np.array(train_labels)
    test_images     = np.array(test_images)
    test_labels     = np.array(test_labels)

    if reshape is True:
        train_images    = train_images.reshape(train_images.shape[0], image_height, image_width, 1)
        test_images     = test_images.reshape(test_images.shape[0], image_height, image_width, 1)

    # test_data   = np.expand_dims(test_data, -1)
    # test_labels = to_categorical(test_labels, num_classes=7)
    # data        = np.expand_dims(data, -1)
    # labels      = to_categorical(labels, num_classes=7)

    return train_images, train_labels, test_images, test_labels


def print_images_info(images, labels):
    print('Train images (*count, *height, *width):')
    print(images.shape)

    print('First Image')
    plt.figure()
    plt.imshow(images[0])
    plt.colorbar()
    plt.grid(False)
    plt.show()

    plt.figure(figsize=(10, 10))
    for i in range(25):
        plt.subplot(5, 5, i+1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(images[i], cmap=plt.cm.binary)
        plt.xlabel(class_names[labels[i]])
    plt.show()
    return
