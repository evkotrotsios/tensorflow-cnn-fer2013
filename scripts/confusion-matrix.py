import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn import metrics

# Possible values 'LOCAL' or 'COLAB'
ENVIRONMENT     = 'LOCAL'
MODEL_NAME      = 'fer2013_ResNet29v2_model.082.h5'
DATASET_NAME    = 'fer2013.csv'
MODEL_PATH      = '../models/resnet/' + MODEL_NAME
DATASET_PATH    = '../fer2013/' + DATASET_NAME
SAVE_MODEL_PATH = '.'

if ENVIRONMENT == 'COLAB':
    DATASET_PATH    = '/content/drive/My Drive/Colab Notebooks/dataset-fer2013/' + DATASET_NAME
    MODEL_PATH      = '/content/drive/My Drive/Colab Notebooks/' + MODEL_NAME
    from google.colab import files
    files.upload()

from load_fer2013 import load_data
from keras.models import load_model
import keras

num_classes         = 7
subtract_pixel_mean = True
emotion_dict        = {0: "Angry", 1: "Disgust", 2: "Fear", 3: "Happy", 4: "Sad", 5: "Surprise", 6: "Neutral"}

model                               = load_model(MODEL_PATH)
x_train, y_train, x_test, y_test    = load_data(DATASET_PATH, False, True)

# Normalize data.
x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255

# If subtract pixel mean is enabled
if subtract_pixel_mean:
    x_train_mean = np.mean(x_train, axis=0)
    x_train -= x_train_mean
    x_test -= x_train_mean

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
# y_test = keras.utils.to_categorical(y_test, num_classes)

predictions = model.predict(x_test)

np.set_printoptions(precision=2)

pred_list = []
actual_list = []

for i in predictions:
    pred_list.append(np.argmax(i))

for i in y_test:
    actual_list.append(i)

# cf = confusion_matrix(actual_list, pred_list)
import itertools

cm = confusion_matrix(actual_list, pred_list, labels=[0, 1, 2, 3, 4, 5, 6])
labels = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
title='Confusion matrix'
print(cm)

plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
plt.title(title)
plt.colorbar()
tick_marks = np.arange(len(labels))
plt.xticks(tick_marks, labels, rotation=45)
plt.yticks(tick_marks, labels)
fmt = 'd'
thresh = cm.max() / 2.
for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    plt.text(j, i, format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.tight_layout()
plt.show()

print(metrics.classification_report(actual_list, pred_list, labels=[0, 1, 2, 3, 4, 5, 6], digits=7))