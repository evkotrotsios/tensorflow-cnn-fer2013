from keras.models import load_model
import matplotlib.pyplot as plt
from load_fer2013 import load_data
import numpy as np
import keras

# import cv2
num_classes = 7
subtract_pixel_mean = True
emotion_dict = {0: "Angry", 1: "Disgust", 2: "Fear", 3: "Happy", 4: "Sad", 5: "Surprise", 6: "Neutral"}

model = load_model('../saved_models/fer2013_ResNet20v1_model.131.h5')

x_train, y_train, x_test, y_test = load_data('../fer2013/fer2013.csv', False, True)

# Normalize data.
x_train = x_train.astype('float32') / 255
x_test = x_test.astype('float32') / 255

# If subtract pixel mean is enabled
if subtract_pixel_mean:
    x_train_mean = np.mean(x_train, axis=0)
    x_train -= x_train_mean
    x_test -= x_train_mean

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
# y_test = keras.utils.to_categorical(y_test, num_classes)

i           = 0
correct     = 0
pred_list   = []

for image in x_test:

    test = np.expand_dims(image, 0)
    prediction = model.predict(test)

    print(str(i) + ". Prediction: " + emotion_dict[int(np.argmax(prediction))] + " --- Real Emotion: " + emotion_dict[int(y_test[i])])

    if int(np.argmax(prediction)) == int(y_test[i]):
        correct = correct + 1

    pred_list.append(np.argmax(prediction))

    i = i + 1

print("Correct: " + str(correct) + " Total: " + str(i))

actual_list = []

for i in y_test:
    actual_list.append(i)

import itertools
from sklearn.metrics import confusion_matrix

cm      = confusion_matrix(y_test, pred_list, labels=[0, 1, 2, 3, 4, 5, 6])
labels  = ['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
title   = 'Confusion matrix'
print(cm)

plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
plt.title(title)
plt.colorbar()
tick_marks = np.arange(len(labels))
plt.xticks(tick_marks, labels, rotation=45)
plt.yticks(tick_marks, labels)
fmt = 'd'
thresh = cm.max() / 2.
for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    plt.text(j, i, format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black")

plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.tight_layout()
plt.show()

exit(0)